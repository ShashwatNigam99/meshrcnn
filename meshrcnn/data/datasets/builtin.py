# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved
"""
This file registers pre-defined datasets at hard-coded paths, and their metadata.
We hard-code metadata for common datasets. This will enable:
1. Consistency check when loading the datasets
2. Use models on these standard datasets directly and run demos,
   without having to download the dataset annotations
We hard-code some paths to the dataset that's assumed to
exist in "./datasets/".
"""
import os
from detectron2.data import DatasetCatalog, MetadataCatalog

from meshrcnn.data.datasets import load_pix3d_json

# {"name" : 'Rack', "color" : [55,25,55], "id" : 2 },
def get_pix3d_metadata():
    meta = [
        {"name" : 'Box', "color" : [255,55,25], "id" : 1 }, 
    ]
    return meta


SPLITS = {
    "pix3d_s1_train": ("pix3d", "pix3d/pix3d_s1_train.json"),
    "pix3d_s1_test": ("pix3d", "pix3d/pix3d_s1_train.json"),
}


def register_pix3d(dataset_name, json_file, image_root, root="datasets"):
    DatasetCatalog.register(
        dataset_name, lambda: load_pix3d_json(json_file, image_root, dataset_name)
    )
    things_ids = [k["id"] for k in get_pix3d_metadata()]
    thing_dataset_id_to_contiguous_id = {k: i for i, k in enumerate(things_ids)}
    thing_classes = [k["name"] for k in get_pix3d_metadata()]
    thing_colors = [k["color"] for k in get_pix3d_metadata()]
    json_file = os.path.join(root, json_file)
    image_root = os.path.join(root, image_root)
    metadata = {
        "thing_classes": thing_classes,
        "thing_dataset_id_to_contiguous_id": thing_dataset_id_to_contiguous_id,
        "thing_colors": thing_colors,
    }
    MetadataCatalog.get(dataset_name).set(
        json_file=json_file, image_root=image_root, evaluator_type="pix3d", **metadata
    )


for key, (data_root, anno_file) in SPLITS.items():
    register_pix3d(key, anno_file, data_root)
